import os
from conan.tools.cmake import CMakeToolchain, CMakeDeps

from environment.conan import UrfConanProject

class UrfCommonCppConan(UrfConanProject):
    name = "urf_common_cpp"
    version = "1.10.0"
    license = "MIT"
    author = "Giacomo Lunghi"
    url = "https://gitlab.com/jamaslab/urf-common-cpp"
    description = "Unified Robotic Framework Common Objects"
    options = {"shared": [True, False]}
    default_options = {"shared": False}

    requires = (
        "spdlog/1.11.0",
        "nlohmann_json/3.9.1",
        "eigen/3.3.9",
    )

    build_requires = (
        "cmake/3.30.5",
    )

    test_requires = (
        "gtest/1.15.0",
    )

    def generate(self):
        tc = CMakeToolchain(self)

        tc.variables["CMAKE_BUILD_TYPE"] = str(self.settings.build_type)
        tc.variables["BUILD_SHARED_LIBS"] = self.options.shared
        if not self.options.shared:
            tc.variables["CMAKE_POSITION_INDEPENDENT_CODE"] = True

        tc.generate()

        deps = CMakeDeps(self)
        deps.generate()


    def imports(self):
        if self.settings.os == "Windows":
            for path in [
                "../Windows/{}/bin/{}".format(self.settings.build_type, self.settings.build_type),
            ]:
                self.copy("*.dll", dst=path, src="lib")
                self.copy("*.pdb", dst=path, src="lib")

    def package_info(self):
        self.cpp_info.libs = ["urf_common"]
        self.cpp_info.includedirs = ["include"]
        self.cpp_info.libdirs = ["lib"]
        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
